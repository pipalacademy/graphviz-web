#!/bin/sh

# performs lint check on all source code
golint -set_exit_status
return_code=$?; if [[ $return_code != 0 ]]; then echo "FAILED: lint check" && exit $return_code; else echo "PASSED: lint check" ; fi
